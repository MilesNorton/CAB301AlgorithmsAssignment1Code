//
// Created by MilesNorton on 25/03/2017.
//
#ifndef BRUTEFORCEMEDIAN_H
#define BRUTEFORCEMEDIAN_H


class BruteForceMedian
{
    public:
        BruteForceMedian(int sizeOfArray);
        ~BruteForceMedian();
        int getMedian(int *array_A, int *basicOperationCount, float *time);
    protected:

    private:
        int median_k;
        int arrayLength_n;
};

#endif // BRUTEFORCEMEDIAN_H
