//
// Created by MilesNorton on 25/03/2017.
//
#include "bruteforcemedian.h"
#include <cmath>
#include <chrono>
using namespace std;

BruteForceMedian::BruteForceMedian(int sizeOfArray) {

    this->arrayLength_n  = sizeOfArray;
    this->median_k = ceil( double(arrayLength_n) / 2 );

}

BruteForceMedian::~BruteForceMedian(){}

int BruteForceMedian::getMedian(int *array_A, int *basicOperationCount, float *time) {

    auto begin = chrono::high_resolution_clock::now();
    for( int i = 0; i <= ( arrayLength_n - 1 ); i++ ) { // run through 0 to N-1  = N iterations

        int numSmaller = 0; // 1
        int numEqual = 0; // 2

        for( int j = 0; j <= ( arrayLength_n - 1); j++ ) { // run through 0 to N-1 = N Iterations

            if ( array_A[j] < array_A[i] ){

                numSmaller = numSmaller + 1; // 3

            }else if(array_A[j] == array_A[i]) {

                numEqual = numEqual + 1; // 4

            }

            *basicOperationCount += 1;

        }
        if( ( ( numSmaller < median_k ) && ( median_k <= ( numSmaller + numEqual ) ) ) ) {

            //seconds
            *time += (chrono::duration_cast<chrono::nanoseconds>(chrono::high_resolution_clock::now()-begin).count())* 0.000000001;

            return array_A[i]; // 4

        }

    }
    return -1; //for fail.

}
