//
// Created by MilesNorton on 25/03/2017.
//
#include <iostream>
#include <stdlib.h>
#include "bruteforcemedian.h"
#include <iomanip>
#include <algorithm>
#include <fstream>

using namespace std;

int main()
{
    int NO_OF_TESTS = 750;
    int *array_A = NULL;
    int basicOperationCount = 0;
     float time;
    int median;

    ofstream executionFile;
    ofstream operationCountFile;

    executionFile.open ("executionTime.txt");
    operationCountFile.open ("operationCount.txt");
    for(int j = 0; j < 750; j++){ // complete 750 different types of arrays

        basicOperationCount = 0;
        array_A = new int[ j + 3 ]; //increase array size

        for (int i = 0; i < NO_OF_TESTS; i++){ // loop through i amount of times for testing array size

            for (int k = 0; k < j + 3; k++) {
                array_A[k] = rand() % (5000) + 1; // generate a value from 0-->5000 in k position
            }

            BruteForceMedian medianAlgorithm( j + 3 ); //initialise class
            median = medianAlgorithm.getMedian(array_A, &basicOperationCount, &time); // run algorithm - brute force median
            medianAlgorithm.~BruteForceMedian(); // free memory

        }
        delete[] array_A; //delete array

        executionFile << time/NO_OF_TESTS <<"\n"; // average time of test
        operationCountFile <<   basicOperationCount/ NO_OF_TESTS  <<"\n"; //average count of test

    }

    executionFile.close();
    operationCountFile.close();

}

